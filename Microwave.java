import java.util.Scanner;
public class Microwave{
	private String size;
	private int numberOfMealsHeated;
	private boolean smart;
	private String color;
	
	public Microwave(String size, int numberOfMealsHeated, boolean smart,String color){
		this.size=size;
		this.numberOfMealsHeated=numberOfMealsHeated;
		this.smart=smart;
		this.color=color;
	}
	
	public void isItSmart(){
		if(this.smart){
			System.out.println("This is a smart microwave");
		}else{
			System.out.println("This is a normal microwave");
		}
	}
	
	public void printSizeAndColor(){
		System.out.println("Size: "+this.size + " Color: "+this.color);
	}
	
	public void addMealsHeated(int numberOfMealsHeated){		
		this.numberOfMealsHeated = this.numberOfMealsHeated + validator(numberOfMealsHeated);
	}
	
	private int validator(int num){
		Scanner sc=new Scanner(System.in);
		if(num<=0){
			System.out.println("Input can't be below 1. Please enter a valid input");
			num=validator(sc.nextInt());
		}
		return num;
	}
	
	public String getSize(){
		return this.size;
	}
	public int getNumberOfMealsHeated(){
		return this.numberOfMealsHeated;
	}
	public boolean getSmart(){
		return this.smart;
	}
	public String getColor(){
		return color;
	}
	public void setSize(String size){
		this.size=size;
	}
	public void setNumberOfMealsHeated(int numberOfMealsHeated){
		this.numberOfMealsHeated=numberOfMealsHeated;
	}
	public void setSmart(boolean smart){
		this.smart=smart;
	}
	public void setColor(String color){
		this.color=color;
	}
	
	
}

