import java.util.Scanner;
public class ApplianceStore{
	public static void main(String[] args){
		Scanner scanner=new Scanner(System.in);
		Microwave[] mws=new Microwave[4];	
		
		for(int i=0;i<mws.length;i++){					
			System.out.println("Enter the size of the microwave");			
			String size=scanner.nextLine(); 
			
			System.out.println("Enter the color");
			String color=scanner.nextLine();
				
			System.out.println("Enter true or false if it is a smart microwave");
			Boolean smart=scanner.nextBoolean();
		
			System.out.println("Enter the number of meals heated");	
			int numberOfMealsHeated=scanner.nextInt();

			mws[i]=new Microwave(size,numberOfMealsHeated,smart,color);
			
			//to move the pointer 
			scanner.nextLine();
			
		}
		
		System.out.println();
		
		// 2 instance methods on the first microwave
		System.out.println("First method isItSmart() output: ");
		mws[0].isItSmart();
		System.out.println("Second method printSizeAndColor() output: ");
		mws[0].printSizeAndColor();
		
		System.out.println();
		
		//Call the instance method on the second appliance
		System.out.println("Instance method on second microwave: ");
		System.out.println("Initial numberOfMealsHeated value before adding 5: "+mws[1].getNumberOfMealsHeated());
		mws[1].addMealsHeated(5);
		System.out.println("After calling the instance method : "+ mws[1].getNumberOfMealsHeated());
		
		System.out.println();
		
		//print last microwave's fields
		System.out.println("Printing last microwave's fields BEFORE update: ");
		System.out.println("Microwave's size field: "+mws[mws.length-1].getSize());
		System.out.println("Microwave's color field: "+mws[mws.length-1].getColor());
		System.out.println("Microwave's smart field: "+mws[mws.length-1].getSmart());
		System.out.println("Microwave's numberOfMealsHeated field: "+mws[mws.length-1].getNumberOfMealsHeated());
		
		//Update the values of the last microwave's fields
		System.out.println("Updating last microwave's fields. Enter new size for the microwave: ");
		mws[mws.length-1].setSize(scanner.nextLine());
		System.out.println("Enter new color for the microwave: ");
		mws[mws.length-1].setColor(scanner.nextLine());
		System.out.println("Enter true or false if it is a smart microwave");
		mws[mws.length-1].setSmart(scanner.nextBoolean());
		System.out.println("Enter the number of meals heated");	
		mws[mws.length-1].setNumberOfMealsHeated(scanner.nextInt());
		
		System.out.println("Printing last microwave's fields AFTER update: ");
		System.out.println("Microwave's size field: "+mws[mws.length-1].getSize());
		System.out.println("Microwave's color field: "+mws[mws.length-1].getColor());
		System.out.println("Microwave's smart field: "+mws[mws.length-1].getSmart());
		System.out.println("Microwave's numberOfMealsHeated field: "+mws[mws.length-1].getNumberOfMealsHeated());
		
	}
}